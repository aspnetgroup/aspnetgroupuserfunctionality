﻿namespace BackendModelLibrary.Model
{
    public class NetworkDto
    {
        public int NetworkID { get; set; }

        public List<NetworkUsersGroupDto>? NetworkUsersGroup { get; set; }

        public List<NetworkDeviceDto>? NetworkDevices { get; set; }

        public List<NetworkDevicesGroupDto>? NetworkDevicesGroup { get; set; }
    }


}