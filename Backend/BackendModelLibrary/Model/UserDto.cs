﻿namespace BackendModelLibrary.Model
{
    public class UserDto
    {
        public int UserID { get; set; }

        public required string UserLogin { get; set; }

        public required string UserPassword { get; set; }
    }


}