﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendService.Model.Entities
{
    [Table("Networks")]
    public class Network
    {
        [Key]
        public int NetworkID { get; set; }


        public List<NetworkUser>? NetworkUsers { get; set; }

        public List<NetworkUsersGroup>? NetworkUsersGroup { get; set; }

        public List<NetworkDevice>? NetworkDevices { get; set; }

        public List<NetworkDevicesGroup>? NetworkDevicesGroup { get; set; }
    }

    [Table("Devices")]
    public class Device
    {
        [Key]
        public int DeviceID { get; set; }

        public required string DeviceCode { get; set; }
    }

    [Table("Users")]
    public class User
    {
        [Key]
        public int UserID { get; set; }

        public required string UserLogin { get; set; }

        public required string UserPassword { get; set; }
    }

    [Table("NetworkDevices")]
    public class NetworkDevice
    {
        [Key]
        public int NetworkDeviceID { get; set; }

        public int NetworkID { get; set; }

        public int DeviceID { get; set; }


        public Network? Network { get; set; }

        public Device? Device { get; set; }
    }

    [Table("NetworkUsers")]
    public class NetworkUser
    {
        [Key]
        public int NetworkUserID { get; set; }

        public int NetworkID { get; set; }

        public int UserID { get; set; }


        public Network? Network { get; set; }

        public User? User { get; set; }
    }

    [Table("NetworkDevicesGroups")]
    public class NetworkDevicesGroup
    {
    }

    [Table("NetworkDevicesAccesses")]
    public class NetworkDevicesAccess
    {
    }

    [Table("NetworkUsersGroups")]
    public class NetworkUsersGroup
    {
    }


}