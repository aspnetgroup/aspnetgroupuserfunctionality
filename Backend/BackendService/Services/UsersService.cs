﻿using BackendCommonLibrary.Interfaces.Services;
using BackendModelLibrary.Model;
using BackendService.DataSources;
using BackendService.Model.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackendService.Services
{
    public class UsersService : IUsersService
    {
        private ILogger Logger { get; set; }

        private BackendContext Context { get; set; }


        public UsersService(ILogger logger, BackendContext context)
        {
            Logger = logger;
            Context = context;
        }

        public async Task<UserDto> GetUserAsync(int userID)
        {
            var user = await Context.Users.FindAsync(userID) ?? throw new KeyNotFoundException($"User with userID {userID}");

            return Convert(user);
        }

        public async Task<IEnumerable<UserDto>> GetUsersAsync()
        {
            var users = await Context.Users.ToListAsync();

            return users.Select(Convert);
        }

        public async Task CreateUserAsync(UserDto userDto)
        {
            var user = Convert(userDto);

            Context.Add(user);
            await Context.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(int userID, UserDto userDto)
        {
            var user = Convert(userDto);

            Context.Attach(user);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(int userID)
        {
            var user = new User()
            {
                UserID = userID,
                UserLogin = "",
                UserPassword = "",
            };

            Context.Remove(user);

            await Context.SaveChangesAsync();
        }

        private UserDto Convert(User user)
        {
            throw new NotImplementedException();
        }

        private User Convert(UserDto user)
        {
            throw new NotImplementedException();
        }
    }
}