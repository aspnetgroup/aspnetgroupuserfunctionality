﻿using System;
using System.Collections.Generic;
using BackendService.Model.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackendService.DataSources;

public class BackendContext : DbContext
{
    public DbSet<Network> Networks { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Device> Devices { get; set; }
    public DbSet<NetworkUser> NetworkUsers { get; set; }
    public DbSet<NetworkDevice> NetworkDevices { get; set; }
    public DbSet<NetworkUsersGroup> NetworkUsersGroups { get; set; }
    public DbSet<NetworkDevicesGroup> NetworkDevicesGroups { get; set; }
    public DbSet<NetworkDevicesAccess> NetworkDevicesAccesses { get; set; }

    public BackendContext()
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.LogTo(Console.WriteLine);
    }
}